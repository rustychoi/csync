use crate::tests_e2e::success::incremental_builds::util::*;
use maplit::*;

// ./
// ├── d0/
// │  ├── d1/
// │  │  ├── d2/
// │  │  └── f1
// │  ├── d4/
// │  │  ├── f3
// │  │  └── f4
// │  ├── d5/
// │  │  ├── d6/
// │  │  └── d7/
// │  └── f2
// ├── d3/
// └── f0
//
// ./d0/
// ./d0/d1/
// ./d0/d1/d2/
// ./d0/d1/f1
// ./d0/d4/
// ./d0/d4/f3
// ./d0/d4/f4
// ./d0/d5/"
// ./d0/d5/d6
// ./d0/d5/d7
// ./d0/d5/d8
// ./d0/f2
// ./d3/
// ./f0

macro_rules! paths {
    () => {
        vec![
            "d0/",
            "d0/d1/",
            "d0/d1/d2/",
            "d0/d1/f1",
            "d0/d4/",
            "d0/d4/f3",
            "d0/d4/f4",
            "d0/d5/",
            "d0/d5/d6/",
            "d0/d5/d7/",
            "d0/d5/d8/",
            "d0/f2",
            "d3/",
            "f0",
        ]
    };
}

//
generate_incremental_build_success_test_func!(
    create_nothing,
    tmpdir!().unwrap(),
    paths!(),
    hashset! {},
    "n2OcPvpSHH6F0D00Npy8ifaX6Sv9UdFPJKwKo1QeoaJAxIEzU9Dc41xLRh8W68j2"
);

//
generate_incremental_build_success_test_func!(
    create_toplevel_file,
    tmpdir!().unwrap(),
    paths!(),
    hashset! {create_file!("f100")},
    "U1NUmqFrUB3ccMG42MMWeFpdIqLFwfivDybQgp787HtdXJxhbsKXj3yDCmDpTLDV"
);

//
generate_incremental_build_success_test_func!(
    create_toplevel_dir,
    tmpdir!().unwrap(),
    paths!(),
    hashset! {create_dir!("d100/")},
    "jGyAECT5JXvvnHJqB0iU08c7S9GDxZDaG70as4bGYr4ylMkkS0WLSiCfatcFA5wp"
);

/*
//
generate_incremental_build_success_test_func!(
    nested_empty_dir,
    tmpdir!().unwrap(),
    paths!(),
    hashset! {delete!("d0/d1/d2/")},
    "B9WlmZZbRThjGwUyypFz33jUcvxRKdH827X3PKzdFxODpaaTFvFRh3HvgW418fTU"
);

//
generate_incremental_build_success_test_func!(
    nested_file,
    tmpdir!().unwrap(),
    paths!(),
    hashset! {delete!("d0/d1/f1")},
    "Jgc99KQ2CifNNeFpTxzMfiAxMNw6aHNvNYq7hGRfMW4wU3fuPPa4XUF1NdU3LQ5s"
);

//
generate_incremental_build_success_test_func!(
    nested_dir_of_files,
    tmpdir!().unwrap(),
    paths!(),
    hashset! {delete!("d0/d4/"), delete!("d0/d4/f3"), delete!("d0/d4/f4")},
    "FpNquL7nH1ycnsWeMvvyUUH1gwRmdUzp5KIYWc45z9mDmlHrgir2LYir18BoNesI"
);

//
generate_incremental_build_success_test_func!(
    nested_dir_of_empty_dirs,
    tmpdir!().unwrap(),
    paths!(),
    hashset! {
        delete!("d0/d5/"),
        delete!("d0/d5/d6/"),
        delete!("d0/d5/d7/"),
        delete!("d0/d5/d8/")
    },
    "FpNquL7nH1ycnsWeMvvyUUH1gwRmdUzp5KIYWc45z9mDmlHrgir2LYir18BoNesI"
);

//
generate_incremental_build_success_test_func!(
    nested_dir_of_all,
    tmpdir!().unwrap(),
    paths!(),
    hashset! {
        delete!("d0/"),
        delete!("d0/d1/"),
        delete!("d0/d1/d2/"),
        delete!("d0/d1/f1"),
        delete!("d0/d4/"),
        delete!("d0/d4/f3"),
        delete!("d0/d4/f4"),
        delete!("d0/d5/"),
        delete!("d0/d5/d6/"),
        delete!("d0/d5/d7/"),
        delete!("d0/d5/d8/"),
        delete!("d0/f2")
    },
    "b1VV1nNCmwPKS2cIu8CFEHgg8HsSa1AOtfhjfzCNr2gEfmPaSrmOc33N1slcb5Im"
);

GykRBuBxIYqJNbRhdoHxtwIbas8u9h0gFFMiDeCa5N0rGKR6PwSC5RIn5UIkt7KR
dgeaLjE9d235gj9V42mDRPPJcdOV8IXnPkSWuS8en7UMzVXeZHyThoazSdaFLfhy
GnN19ZDb84WRvUgm2je9BGrumC9TX1zcSERR8TPQx0kqGhYNNhvCevQAeAoabk4F
vfAmJSJWkPcxDNOEVmUE46VR5sMUIk7UuxWizSeFP0dMbMd2dtsJljFTbpVQDPiZ
gl2I0zdLs0QT1WxYzGLA3ULrjbrsuT25gWOMcR572roPL8ZGNhxwb3zJfZehMDo1
BXgIxP6V9qXWb2cWblJt3OFnytqt8MfNm2cm8DxogsJvQAsTBAspq8Rl4j4O1uAp
9PNbSgna9lpZ1PVkFY8dhbbNysrsnDp1fDTxNS5jWHU7I7G0nx0yzx46LAUlLVUh
2CdTflnzKYDzfE8RNqFrmGeu0zIgh9V1oj5lKAvawBjxfRy7WrmNAKUpn0BvkOax
JXXxdfXr5kBE4vNgFd3ue3rHXyu70IH2PQSu3fbjsqoQec34ScquLZZTFu6c478s
3m0LDjDkqIH8543rWg0D7iIjdD8b2BX2eRD2QJHTJrziKco86hMKV1yOQ7Q8NXHZ
hehDTgXAnmQUEdLtYIslYfxwRWx0PN0OirCSddO5ZIoZKTWSxJUsLJWFsqhKRBJF
pTRMah4lETjCYcwT7AXHhmHWrxuBWIL9h1iXfC7UL5bhosgBnB9qKo8Y4k4FqFCj

*/
